import logo from "./logo.svg";
import "./App.css";
import BT_phone_hook from "./Component/BT_phone_hook";

function App() {
  return (
    <div>
      <BT_phone_hook />
    </div>
  );
}

export default App;
