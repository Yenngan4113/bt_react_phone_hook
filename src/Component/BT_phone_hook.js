import React, { useCallback, useState } from "react";
import CartModal from "./CartModal";
import DetailPhone from "./DetailPhone";
import PhoneList from "./PhoneList";
import SelectedCart from "./SelectedCart";

export default function BT_phone_hook() {
  const [detailPhone, setShowURL] = useState(false);
  const [cart, setCart] = useState([]);
  const [isShowModal, setShowModal] = useState(false);

  const handleShowDetailUseCallBack = useCallback((data) => {
    setShowURL(data);
  }, []);
  const handleAddItem = useCallback((data) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.maSP == data.maSP;
    });
    if (index == -1) {
      let newItem = { ...data, quantity: 1 };
      cloneCart.push(newItem);
    } else {
      cloneCart[index].quantity++;
    }

    setCart(cloneCart);
  });
  const handleDelete = (data) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.maSP == data.maSP;
    });
    if (index != -1) {
      cloneCart.splice(index, 1);
    }
    setCart(cloneCart);
  };
  const handleInscreaseQuantity = (data) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.maSP == data.maSP;
    });
    if (index != -1) {
      data.quantity++;
    }
    setCart(cloneCart);
  };
  const handleDescreaseQuantity = (data) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.maSP == data.maSP;
    });
    if (index != -1 && cloneCart[index].quantity > 1) {
      data.quantity--;
    } else {
      cloneCart.splice(index, 1);
    }
    setCart(cloneCart);
  };
  return (
    <div>
      <CartModal
        Cart={cart}
        isShowModal={isShowModal}
        setShowModal={setShowModal}
        handleDelete={handleDelete}
        handleDescreaseQuantity={handleDescreaseQuantity}
        handleInscreaseQuantity={handleInscreaseQuantity}
      />
      <SelectedCart Cart={cart} setShowModal={setShowModal} />
      <PhoneList
        handleShowDetailUseCallBack={handleShowDetailUseCallBack}
        handleAddItem={handleAddItem}
      />
      {detailPhone && <DetailPhone DetailPhone={detailPhone} />}
    </div>
  );
}
