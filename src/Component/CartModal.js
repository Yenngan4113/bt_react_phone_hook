import React from "react";
import { Button, Modal } from "antd";

export default function CartModal({
  Cart,
  isShowModal,
  setShowModal,
  handleDelete,
  handleInscreaseQuantity,
  handleDescreaseQuantity,
}) {
  let totalPrice = Cart.reduce((acc, curr) => {
    return acc + curr.quantity * curr.giaBan;
  }, 0);
  let totalQuantity = Cart.reduce((acc, curr) => {
    return acc + curr.quantity;
  }, 0);
  return (
    <div>
      <Modal
        title={`Giỏ hàng (${totalQuantity})`}
        visible={isShowModal}
        onOk={() => {
          setShowModal(false);
        }}
        onCancel={() => {
          setShowModal(false);
        }}
        width={1000}
      >
        <table className="table-auto text-center">
          <thead>
            <tr className=" font-bold">
              <td className="px-4 py-2">Mã sản phẩm</td>
              <td className="px-4 py-2">Hình ảnh</td>
              <td className="px-4 py-2">Tên sản phẩm</td>
              <td className="px-4 py-2">Số lượng</td>
              <td className="px-4 py-2">Đơn giá</td>
              <td className="px-4 py-2">Thành tiền</td>
              <td className="px-4 py-2"></td>
            </tr>
          </thead>
          <tbody>
            {Cart.map((item, index) => {
              return (
                <tr key={index}>
                  <td className="px-4 py-2">{item.maSP}</td>
                  <td className="px-4 py-2">
                    <img src={item.hinhAnh} className=" w-24" />
                  </td>
                  <td className="px-4 py-2">{item.tenSP}</td>
                  <td className="px-4 py-2">
                    <button
                      className=" bg-red-600 text-white rounded py-2 px-3 mr-2"
                      onClick={() => {
                        handleDescreaseQuantity(item);
                      }}
                    >
                      {item.quantity == 1 ? "Xóa" : "-"}
                    </button>
                    {item.quantity}
                    <button
                      className=" bg-green-600 text-white rounded py-2 px-3 ml-2"
                      onClick={() => {
                        handleInscreaseQuantity(item);
                      }}
                    >
                      +
                    </button>
                  </td>
                  <td className="px-4 py-2">
                    {new Intl.NumberFormat("vi-VI").format(item.giaBan)}VND
                  </td>
                  <td className="px-4 py-2">
                    {new Intl.NumberFormat("vi-VI").format(
                      item.giaBan * item.quantity
                    )}
                    VND
                  </td>
                  <td className="px-4 py-2">
                    <button
                      className=" bg-red-600 text-white rounded py-2 px-3"
                      onClick={() => {
                        handleDelete(item);
                      }}
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              );
            })}
            <tr>
              <td
                colSpan={5}
                className="text-left font-bold text-xl text-red-700 py-2"
              >
                Thành tiền
              </td>

              <td className="font-bold text-xl text-red-700 py-2">
                {new Intl.NumberFormat("vi-VI").format(totalPrice)}VND
              </td>
            </tr>
          </tbody>
        </table>
      </Modal>
    </div>
  );
}
