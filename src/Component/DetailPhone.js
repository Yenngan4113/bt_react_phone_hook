import React from "react";

export default function DetailPhone({ DetailPhone }) {
  return (
    <div className="container mx-auto flex mt-12">
      <img src={DetailPhone.hinhAnh} className=" w-96"></img>
      <div className="col-8 my-5">
        <table className="table-auto">
          <thead>
            <tr>
              <th className="px-4 py-2 col-span-2">Thông số kỹ thuật</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className=" px-4 py-2">Màn hình</td>
              <td className=" px-4 py-2">{DetailPhone.manHinh}</td>
            </tr>
            <tr>
              <td className=" px-4 py-2">Hệ điều hành</td>
              <td className=" px-4 py-2">{DetailPhone.heDieuHanh}</td>
            </tr>
            <tr>
              <td className=" px-4 py-2">Camera trước</td>
              <td className=" px-4 py-2">{DetailPhone.cameraTruoc}</td>
            </tr>
            <tr>
              <td className=" px-4 py-2">Camera sau</td>
              <td className=" px-4 py-2">{DetailPhone.cameraSau}</td>
            </tr>
            <tr>
              <td className=" px-4 py-2">Ram</td>
              <td className=" px-4 py-2">{DetailPhone.ram}</td>
            </tr>
            <tr>
              <td className=" px-4 py-2">Rom</td>
              <td className=" px-4 py-2">{DetailPhone.rom}</td>
            </tr>
            <tr>
              <td className=" px-4 py-2">Giá bán</td>
              <td className=" px-4 py-2 font-bold text-red-600">
                {new Intl.NumberFormat("vi-VI").format(DetailPhone.giaBan)}VND
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
