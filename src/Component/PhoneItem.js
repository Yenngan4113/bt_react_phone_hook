import React, { memo } from "react";

function PhoneItem({ Item, handleShowDetailUseCallBack, handleAddItem }) {
  return (
    <div>
      <div className="max-w-sm rounded overflow-hidden shadow-lg py-2">
        <img className="w-full h-96" src={Item.hinhAnh} alt={Item.hinhAnh} />
        <div className="px-6 py-4">
          <div className="font-bold text-xl mb-2">{Item.tenSP}</div>
          <p className=" text-red-700 font-bold">
            Price: {new Intl.NumberFormat("vi-VI").format(Item.giaBan)} VND
          </p>
          <p className="text-gray-700 text-base">
            Camera Trước: {Item.cameraTruoc}
            <br />
            Camera Sau: {Item.cameraSau}
          </p>
        </div>
        <div className="px-6 pt-4 pb-2 flex justify-around">
          <button
            className=" bg-yellow-500 text-white px-8 py-2 rounded-md hover:bg-yellow-300 transition duration-500"
            onClick={() => {
              handleShowDetailUseCallBack(Item);
            }}
          >
            More detail
          </button>
          <button
            className=" bg-green-800 text-white px-8 py-2 rounded-md hover:bg-green-500 transition duration-500"
            onClick={() => {
              handleAddItem(Item);
            }}
          >
            Add to cart
          </button>
        </div>
      </div>
    </div>
  );
}
export default React.memo(PhoneItem);
