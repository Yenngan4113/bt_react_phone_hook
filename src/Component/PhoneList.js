import React from "react";
import { data } from "../dataPhone";
import DetailPhone from "./DetailPhone";
import PhoneItem from "./PhoneItem";

export default function PhoneList({
  setNumber,
  handleShowDetailUseCallBack,
  handleAddItem,
}) {
  return (
    <div className="container m-auto">
      <div className="grid grid-cols-3">
        {data.map((item, index) => {
          return (
            <PhoneItem
              key={index}
              Item={item}
              handleShowDetailUseCallBack={handleShowDetailUseCallBack}
              handleAddItem={handleAddItem}
            />
          );
        })}
      </div>
    </div>
  );
}
