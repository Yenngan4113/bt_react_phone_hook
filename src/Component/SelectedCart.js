import React from "react";

export default function SelectedCart({ Cart, setShowModal }) {
  let itemQuantity = Cart.reduce((acc, currentValue) => {
    return acc + currentValue.quantity;
  }, 0);
  return (
    <div className="text-right">
      <button
        className=" rounded-lg py-2 px-2  text-red-600 font-medium border-2 mr-24 mt-6"
        onClick={() => {
          setShowModal(true);
        }}
      >
        Giỏ hàng({itemQuantity})
      </button>
    </div>
  );
}
